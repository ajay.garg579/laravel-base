<?php
namespace App\helper;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;

class RModel extends Model
{

    public $error;

    public function save(array $options = [])
    {
        try {

            if ($this->hasColumn('created_by_id')) {

                if (empty($this->created_by_id)) {
                    $this->created_by_id = Auth::id();
                }
            }
            if ($this->hasColumn('created_at')) {
                $this->created_at = date("Y-m-d H:i:s");
            }

            if ($this->hasColumn('updated_at')) {
                $this->updated_at = date("Y-m-d H:i:s");
            }
            return parent::save($options);
        } catch (\Illuminate\Database\QueryException $e) {
            $error = "Something went wrong";
            if (env('APP_ENV') !== "production") {
                $error = $e->getMessage();
            }
            $this->error = $error;
            return false;
        }
    }

    public function getErrorSummary()
    {
        return $this->error;
    }

    public function loadData($request, $attributes)
    {
        $baseClass = class_basename(get_class($this));

        if (! empty($attributes)) {
            foreach ($attributes as $attribute => $validation) {
                if (isset($request->$baseClass[$attribute]) /* && ! empty($request->$baseClass[$attribute]) || $request->$baseClass[$attribute] == 0 */) {
                    $this->$attribute = $request->$baseClass[$attribute];
                }
            }
            return true;
        }

        return false;
    }

    public function uploadFile($request, $model, $attribute, $allowed_extentions = 'mimes:jpeg,png,jpg,gif,webp,svg|max:2048')
    {
        $baseClass = class_basename(get_class($model));
        $rules = array(
            $attribute => 'required|' . $allowed_extentions
        );

        $validator = Validator::make(isset($request->all()[$baseClass]) ? $request->all()[$baseClass] : [], $rules);

        if ($validator->fails()) {
            return $validator->errors()->all()[0];
        } else {

            if (isset($request->$baseClass[$attribute])) {
                $extension = $request->$baseClass[$attribute]->extension();
                $imageName = time() . '.' . $extension;

                $response = $request->$baseClass[$attribute]->move(storage_path('images'), $imageName);
                if ($response) {
                    $this->$attribute = $imageName;
                    return true;
                }
            }
        }
        return false;
    }

    public function hasColumn($column)
    {
        if (Schema::hasColumn($this->table, $column)) {

            return true;
        }
        return false;
    }
}
