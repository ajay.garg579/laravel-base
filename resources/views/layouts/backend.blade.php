<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }}</title>


    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <link rel="shortcut icon" href="{{ asset('public/images/favicon.png') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/fontsawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
    <link rel="stylesheet" href="assets/css/responsive.css">
<!-- Scripts -->
<script src="{{ asset('public/js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito"
	rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app">
		@include('layouts.backend-header');
		@include('layouts.backend-sidebar');
		<!-- Main Panel -->
		<div class="main-panel">
			<div class="content-wrapper">
				<main class="py-4">@yield('content')</main>

			</div>
			<!-- End Content Wrapper -->
			<!-- Footer -->
			<footer class="footer">
				<div class="card">
					<div class="card-body">
						<div
							class="d-sm-flex justify-content-center justify-content-sm-between">
							<span
								class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright
								� {{ date('Y') }} <a href="https://www.rjaytech.com/" class="text-muted"
								target="_blank">RjayTech</a>. All rights reserved.
							</span> <span
								class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center text-muted">Hand-crafted
								&amp; made with <i class="fas fa-heart text-danger"></i>
							</span>
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer -->
		</div>
		<!-- End Main Panel -->

	</div>
	</div>

	<!-- Scripts -->
	<script src="{{ asset('public/js/jquery-3.3.1.slim.min.js') }}"></script>
	<script src="{{ asset('public/js/popper.min.js') }}"></script>
	<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/js/sweetalert.min.js') }}"></script>
	<script src="{{ asset('public/js/chart.min.js') }}"></script>
	<script src="{{ asset('public/js/chart.js') }}"></script>
	<script src="{{ asset('public/js/custom.js') }}"></script>

	</div>
</body>
</html>
