<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Routing\Controller as BaseController;
use App\helper\BaseHelper;
use Illuminate\Support\Facades\Auth;
use App\User;





class Controller extends BaseController
{
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public $layout = 'backend';
    
    public  $left_menu = [];
    
 
    
    public function callAction($method, $parameters)
    {
        // code that runs before any action
        if (in_array($method, ['method1', 'method2'])) {
            // trigger validation
        }
        
       $this->setlayout();
       
       echo "<pre>";
       
       print_r($this->renderNav());
       
       
        return parent::callAction($method, $parameters);
    }
    
    
    public function setlayout()
    {
        if(Auth::guest()){
            $this->layout = 'backend';
        }else{
            if(User::isUser()){
                $this->layout='front';
            }
        }
        View::share('layout', $this->layout);
    }
    
    public static function addmenu($label, $link, $icon, $visible = null, $list = null)
    {
        if (! $visible)
            return null;
            $item = [
                'label' => '<i
                            class="fa fa-' . $icon . '"></i> <span>' . $label . '</span>',
                'url' => [
                    $link
                ]
            ];
            if ($list != null) {
                $item['options'] = [
                    'class' => 'menu-list'
                ];
                
                $item['items'] = $list;
            }
            
            return $item;
    }
    
    public function renderNav()
    {
        $this->left_menu = [
            self::addMenu('User', '//dashboard/index', 'home', Auth::guest()),
            'User' => self::addMenu('user', '#', 'tasks', ! User::isAdmin(), [
                self::addMenu('Driver', '//user/driver', 'car', User::isAdmin()),
                self::addMenu('User', '//user/user', 'users', User::isAdmin())
            ]),
            self::addMenu('Promo Code', '//promo-code', 'list',  Auth::guest()),
       
           
        ];
        
        
        return $this->left_menu;
    }
   
   
    
    
    
    
    
     
}
