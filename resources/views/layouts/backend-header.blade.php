<div
	class="body-wrapper d-flex justify-content-between flex-column min-vh-100">
	<!-- Navbar -->
	<nav class="navbar p-0 fixed-top">
		<div class="navbar-brand-wrapper d-flex justify-content-center">
			<div
				class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100 pl-4 pr-4">
				<a href="index.html" class="navbar-brand brand-logo"><img
					src="assets/images/logo-full.png" alt="Logo" class="img-fluid"></a>
				<a href="index.html" class="navbar-brand brand-logo-mini d-none"><img
					src="assets/images/logo.png" alt="Logo" class="img-fluid"></a>
				<button class="navbar-toggler" type="button" data-toggle="minimize">
					<span class="fa fa-bars"></span>
				</button>
			</div>
		</div>
		<div
			class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
			<ul class="navbar-nav mr-lg-2 d-flex flex-row align-items-stretch">
				<li class="nav-item nav-profile dropdown"><a
					href="javascript:void(0)" data-toggle="dropdown"
					id="profileDropdown" class="nav-link"> <img
						src="assets/images/rajat_bansal.jpg" alt="profile"> <span
						class="nav-profile-name">RaJat BaNsal</span>
				</a>
					<div class="dropdown-menu dropdown-menu-right navbar-dropdown">
						<a href="javascript:void(0);" class="dropdown-item"><i
							class="fas fa-cog"></i> Setting</a> <a href="javascript:void(0);"
							class="dropdown-item"><i class="fas fa-sign-out-alt"></i> Logout</a>
					</div></li>
				<li class="nav-item nav-user-status">
					<p class="mb-0">Last login was 23 hours ago.</p>
				</li>
			</ul>
			<ul
				class="navbar-nav navbar-nav-right ml-auto d-flex flex-row align-items-stretch">
				<li class="nav-item nav-date"><a
					class="nav-link d-flex justify-content-center align-items-center"
					href="javascript:void(0);">
						<h6 class="date mb-0">Today : Mar 23</h6> <i
						class="fas fa-calendar"></i>
				</a></li>
				<li class="nav-item dropdown"><a href="javascript:void(0)"
					class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center"
					id="messageDropdown" data-toggle="dropdown"> <i class="fas fa-cog"></i>
						<span class="count"></span>
				</a>
					<div
						class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
						aria-labelledby="messageDropdown">
						<p class="mb-0 font-weight-normal text-left dropdown-header">Messages</p>
						<a class="dropdown-item preview-item">
							<div class="preview-thumbnail">
								<img src="assets/images/face4.jpg" alt="Profile Image"
									class="profile-pic">
							</div>
							<div class="preview-item-content flex-grow">
								<h6 class="preview-subject ellipsis font-weight-normal">David
									Grey</h6>
								<p class="font-weight-light small-text text-muted mb-0">The
									meeting is cancelled</p>
							</div>
						</a> <a class="dropdown-item preview-item">
							<div class="preview-thumbnail">
								<img src="assets/images/face2.jpg" alt="Profile Image"
									class="profile-pic">
							</div>
							<div class="preview-item-content flex-grow">
								<h6 class="preview-subject ellipsis font-weight-normal">Tim Cook</h6>
								<p class="font-weight-light small-text text-muted mb-0">New
									product launch</p>
							</div>
						</a> <a class="dropdown-item preview-item">
							<div class="preview-thumbnail">
								<img src="assets/images/face3.jpg" alt="Profile Image"
									class="profile-pic">
							</div>
							<div class="preview-item-content flex-grow">
								<h6 class="preview-subject ellipsis font-weight-normal">Johnson</h6>
								<p class="font-weight-light small-text text-muted mb-0">Upcoming
									board meeting</p>
							</div>
						</a>
					</div></li>
				<li class="nav-item dropdown mr-0"><a
					class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center"
					id="notificationDropdown" href="#" data-toggle="dropdown"> <i
						class="fas fa-bell mx-0"></i> <span class="count"></span>
				</a>
					<div
						class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
						aria-labelledby="notificationDropdown">
						<p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
						<a class="dropdown-item preview-item">
							<div class="preview-thumbnail">
								<div class="preview-icon bg-success">
									<i class="fas fa-exclamation-triangle mx-0"></i>
								</div>
							</div>
							<div class="preview-item-content">
								<h6 class="preview-subject font-weight-normal">Application Error</h6>
								<p class="font-weight-light small-text mb-0 text-muted">Just now</p>
							</div>
						</a> <a class="dropdown-item preview-item">
							<div class="preview-thumbnail">
								<div class="preview-icon bg-warning">
									<i class="fas fa-cog mx-0"></i>
								</div>
							</div>
							<div class="preview-item-content">
								<h6 class="preview-subject font-weight-normal">Settings</h6>
								<p class="font-weight-light small-text mb-0 text-muted">Private
									message</p>
							</div>
						</a> <a class="dropdown-item preview-item">
							<div class="preview-thumbnail">
								<div class="preview-icon bg-info">
									<i class="fas fa-user mx-0"></i>
								</div>
							</div>
							<div class="preview-item-content">
								<h6 class="preview-subject font-weight-normal">New user
									registration</h6>
								<p class="font-weight-light small-text mb-0 text-muted">2 days
									ago</p>
							</div>
						</a>
					</div></li>
			</ul>
			<button
				class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
				type="button" data-toggle="offcanvas">
				<i class="fas fa-bars"></i>
			</button>
		</div>
	</nav>
	<!-- End Navbar -->

	<div class="container-fluid page-body-wrapper">

		<!-- Theme Setting Right Sidebar -->
		<div class="theme-setting-wrapper">
			<div id="settings-trigger">
				<i class="fas fa-cog"></i>
			</div>
		</div>
		<div id="theme-settings" class="settings-panel">
			<div
				class="settings-heading d-flex justify-content-between align-items-center">
				<p class="mb-0 text-uppercase">SIDEBAR SKINS</p>
				<i class="fas fa-times settings-close"></i>
			</div>
			<div class="sidebar-bg-options selected" id="sidebar-light-theme">
				<div class="img-ss rounded-circle bg-light border mr-3"></div>
				Light
			</div>
			<div class="sidebar-bg-options" id="sidebar-dark-theme">
				<div class="img-ss rounded-circle bg-dark border mr-3"></div>
				Dark
			</div>
			<p class="settings-heading mt-2 text-uppercase">HEADER SKINS</p>
			<div class="color-tiles mx-0 px-4">
				<div class="tiles bg-success"></div>
				<div class="tiles bg-warning"></div>
				<div class="tiles bg-danger"></div>
				<div class="tiles bg-info"></div>
				<div class="tiles bg-dark"></div>
				<div class="tiles bg-default"></div>
			</div>
		</div>