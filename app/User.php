<?php
/**
 *@copyright : RjayTech Pvt. Ltd. < www.rjaytech.com >
 *@author     : Ajay Garg < ajay.garg579@gmail.com >
 
 All Rights Reserved.
 
 */
namespace App;

use Illuminate\Notifications\Notifiable;
use App\helper\RAuthenticable;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Str;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class User extends RAuthenticable
{
    use Notifiable;

    const USER_ROLE_ADMIN = 1;

    const USER_ROLE_USER = 2;

    const USER_ROLE_MANAGER = 3;

    

    const STATUS_WORKING = 1;

    const STATUS_BLOCK = 2;

    const STATUS_CANCEL = 3;

    const RESET_KEY = 16;

    const EMAIL_NOT_VERIFIED = 0;

    const EMAIL_VERIFIED = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];
    

    

    public function getResetUrl()
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < USER::RESET_KEY; $i ++) {
            $key .= $keys[array_rand($keys)];
        }

        $this->change_password_token = $key;
        if ($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function getProfileFileAttribute($value)
    {
        if ($value != '') {
            return url('/') . '/public/uploads/' . $value;
        }
        return $value;
    }

    public static function getStatus($id = null)
    {
        $list = array(
            self::STATUS_WORKING => "WORKING",
            self::STATUS_BLOCK => "BLOCKED",
            self::STATUS_CANCEL => "CANCELED"
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }

    public static function getUserRole($id = null)
    {
        $list = array(
            self::USER_ROLE_ADMIN => "Admin",
            self::USER_ROLE_USER => "User",
            self::USER_ROLE_MANAGER => "Manager",
        );
        if ($id === null)
            return $list;
        return isset($list[$id]) ? $list[$id] : 'Not Defined';
    }

   

    public function getVerified()
    {
        return url("/user/confirm-email/{$this->activation_key}");
    }

    public static function isAdmin()
    {
        $user = Auth::user();
        if ($user == null)
            return false;

        return ($user->isWorked() && $user->user_role_id == User::USER_ROLE_ADMIN);
    }

    public static function isUser()
    {
        $user = Auth::user();
        if ($user == null)
            return false;

            return ($user->isWorked() && $user->user_role_id == User::USER_ROLE_USER);
    }
    
    
    
    public static function isManger()
    {
        $user = Auth::user();
        if ($user == null)
            return false;
            
            return ($user->isWorked && $user->user_role_id == User::USER_ROLE_MANAGER);
    }

    public function isWorked()
    {
        return ($this->status_id == User::STATUS_WORKING);
    }

    public function generateAuthKey()
    {
        $this->activation_key = Str::random(32);
    }
    
    public function generateAccessToken()
    {
        $this->api_token = Str::random(32);
    }
}

