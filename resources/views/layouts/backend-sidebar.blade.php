
<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">
		<li class="nav-item"><a href="index.php" class="nav-link"> <i
				class="fas fa-desktop menu-icon"></i> <span class="menu-title">Dashboard</span>
				<div class="badge badge-danger">new</div>
		</a></li>
		<li class="nav-item"><a href="widgets.php" class="nav-link"> <i
				class="fas fa-archive menu-icon"></i> <span class="menu-title">Widgets</span>
		</a></li>
		<li class="nav-item"><a href="#uiBasic" class="nav-link"
			data-toggle="collapse" aria-expanded="false" aria-controls="uiBasic">
				<i class="fas fa-clipboard-list menu-icon"></i> <span
				class="menu-title">UI Elements</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="uiBasic">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Accordions</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Buttons</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Badges</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Breadcrumbs</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Dropdowns</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Modals</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Progress
							bar</a></li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Pagination</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Tabs</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Typography</a>
					</li>
					<li class="nav-item"><a href="javascript:void(0);" class="nav-link">Tooltips</a>
					</li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#ui-advanced" aria-expanded="false"
			aria-controls="ui-advanced"> <i class="fas fa-cog menu-icon"></i> <span
				class="menu-title">Advanced UI</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="ui-advanced">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Dragula</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Clipboard</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Context
							menu</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Sliders</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Carousel</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Colcade</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Loaders</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#form-elements" aria-expanded="false"
			aria-controls="form-elements"> <i
				class="fas fa-file-signature menu-icon"></i> <span
				class="menu-title">Form elements</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="form-elements">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Basic
							Elements</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Advanced
							Elements</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Validation</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Wizard</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#editors" aria-expanded="false"
			aria-controls="editors"> <i class="fas fa-keyboard menu-icon"></i> <span
				class="menu-title">Editors</span> <i class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="editors">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Text
							editors</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Code
							editors</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#charts" aria-expanded="false"
			aria-controls="charts"> <i class="fas fa-chart-pie menu-icon"></i> <span
				class="menu-title">Charts</span> <i class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="charts">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">ChartJs</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Morris</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Flot</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Google
							charts</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Sparkline
							js</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">C3
							charts</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Chartists</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">JustGage</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#tables" aria-expanded="false"
			aria-controls="tables"> <i class="fas fa-border-all menu-icon"></i> <span
				class="menu-title">Tables</span> <i class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="tables">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Basic
							table</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Data
							table</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Js-grid</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Sortable
							table</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" href="javascript:void(0);"> <i
				class="fas fa-fire-alt menu-icon"></i> <span class="menu-title">Popups</span>
		</a></li>
		<li class="nav-item"><a class="nav-link"
			href="../../pages/ui-features/notifications.html"> <i
				class="fas fa-bell menu-icon"></i> <span class="menu-title">Notifications</span>
		</a></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#icons" aria-expanded="false"
			aria-controls="icons"> <i class="fas fa-compass menu-icon"></i> <span
				class="menu-title">Icons</span> <i class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="icons">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Flag
							icons</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Mdi
							icons</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Font
							Awesome</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Simple
							line icons</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Themify
							icons</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#maps" aria-expanded="false"
			aria-controls="maps"> <i class="fas fa-map-marked-alt menu-icon"></i>
				<span class="menu-title">Maps</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="maps">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Mapael</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Vector
							Map</a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">Google
							Map</a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#auth" aria-expanded="false"
			aria-controls="auth"> <i class="fas fa-user-plus menu-icon"></i> <span
				class="menu-title">User Pages</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="auth">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Login </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Login 2 </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Register </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Register 2 </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Lockscreen </a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#error" aria-expanded="false"
			aria-controls="error"> <i class="fas fa-bug menu-icon"></i> <span
				class="menu-title">Error pages</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="error">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							404 </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							500 </a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#general-pages" aria-expanded="false"
			aria-controls="general-pages"> <i
				class="fas fa-external-link-alt menu-icon"></i> <span
				class="menu-title">General Pages</span> <i
				class="fas fa-plus menu-arrow"></i>
		</a>
			<div class="collapse" id="general-pages">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Blank Page </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Profile </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							FAQ </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							FAQ 2 </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							News grid </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Timeline </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Search Results </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void(0);">
							Portfolio </a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" data-toggle="collapse"
			aria-expanded="false" href="#e-commerce" aria-expanded="false"
			aria-controls="e-commerce"> <i class="fas fa-briefcase menu-icon"></i>
				<span class="menu-title">E-commerce</span> <i
				class="fas fa-menu-arrow"></i>
		</a>
			<div class="collapse" id="e-commerce">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"><a class="nav-link" href="javascript:void();">
							Invoice </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void();">
							Pricing Table </a></li>
					<li class="nav-item"><a class="nav-link" href="javascript:void();">
							Orders </a></li>
				</ul>
			</div></li>
		<li class="nav-item"><a class="nav-link" href="javascript:void(0);"> <i
				class="fas fa-envelope-open-text menu-icon"></i> <span
				class="menu-title">E-mail</span>
		</a></li>
		<li class="nav-item"><a class="nav-link" href="javascript:void(0);"> <i
				class="fas fa-calendar-alt menu-icon"></i> <span class="menu-title">Calendar</span>
		</a></li>
		<li class="nav-item"><a class="nav-link" href="javascript:void(0);"> <i
				class="fas fa-list-alt menu-icon"></i> <span class="menu-title">Todo
					List</span>
		</a></li>
		<li class="nav-item"><a class="nav-link" href="javascript:void(0);"> <i
				class="fas fa-image menu-icon"></i> <span class="menu-title">Gallery</span>
		</a></li>
		<li class="nav-item"><a class="nav-link" href="javascript:void(0);"> <i
				class="fas fa-file-medical-alt menu-icon"></i> <span
				class="menu-title">Documentation</span>
		</a></li>
	</ul>
</nav>