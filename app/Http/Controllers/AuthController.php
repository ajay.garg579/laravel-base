<?php
/**
 *@copyright : RjayTech Pvt. Ltd. < www.rjaytech.com >
 *@author     : Ajay Garg < ajay.garg579@gmail.com >
 
 All Rights Reserved.
 
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function addAdmin()
    {
        $user = User::all();
        if ($user->isEmpty()) {
            return view('frontend.auth.add-admin');
        } else {
            return redirect('login');
        }
    }
    
    public function saveAdmin(Request $request)
    {
        $user = User::all();
        if ($user->isEmpty()) {
            $rules = array(
                'email' => 'required|email',
                'password' => 'required|min:8',
                'confirm_password' => 'required|same:password',
                'full_name' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withInput()->withErrors($validator);
            } else {
                $user = new User();
                $user->email = $request->input('email');
                $user->password = bcrypt($request->input('password'));
                $user->full_name = $request->input('full_name');
                $user->role_id = User::ROLE_ADMIN;
                $user->state_id = User::STATE_ACTIVE;
                if ($user->save()) {
                    return redirect('login')->with('success', 'Please login');
                } else {
                    return Redirect::back()->withInput()->with('error', 'Some error occured. Please try again later');
                }
            }
        } else {
            return redirect('login')->with('error', 'Admin already exist');
        }
    }

    
    public function saveUser(Request $request)
    {
        $rules = array(
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'full_name' => 'required',
            'age' => 'integer|min:18|max:100',
            'contact_no' => 'integer|digits_between:5,15|unique:users,contact_no',
            'profile_file' => 'image|mimes:jpeg,png,jpg|max:10000'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $user = new User();
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->full_name = $request->input('full_name');
            $user->role_id = User::ROLE_USER;
            $user->state_id = User::STATE_INACTIVE;
            $user->contact_no = $request->input('contact_no');
            $user->age = $request->input('age');
            $user->address = $request->input('address');
            $user->generateAuthKey();
            if ($request->hasFile('profile_file')) {
                $icon = date('Ymd') . '_' . time() . '.' . $request->file('profile_file')->getClientOriginalExtension();
                $request->profile_file->move(public_path('uploads'), $icon);
                $user->profile_file = $icon;
            }
            if ($user->save()) {
                $user = EmailQueue::add([
                    'to' => $user->email,
                    'from' => env('MAIL_FROM_ADDRESS'),
                    'subject' => 'Your Email Verification Link',
                    'view' => 'email.user-verification',
                    'model' => [
                        'user' => $user
                    ]
                ]);
                return redirect('login')->with('success', 'Mail has been sent to you with a verification link. Please Verify your email after login');
            } else {
                dd($user->getErrorSummary());
                return Redirect::back()->withInput()->with('error', $user->getErrorSummary());
            }
        }
    }
    
    public function actionLogin(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $user = User::where('email', $request->input('email'))->first();
            if (! empty($user)) {
                if ($user->state_id == User::STATE_ACTIVE) {
                    $credentials = $request->only('email', 'password');
                    if (Auth::attempt($credentials)) {
                        $user->generateAccessToken();
                        $user->save();
                        LoginHistory::add($request, LoginHistory::TYPE_WEB, LoginHistory::STATE_SUCCESS, $user->id);
                        if ($user->role_id == User::ROLE_ADMIN) {
                            return redirect('admin')->with('success', 'Login succesfully');
                        } elseif ($user->role_id == User::ROLE_PUBLISHER) {
                            return redirect('publisher')->with('success', 'Login succesfully');
                        } elseif ($user->role_id == User::ROLE_USER) {
                            return redirect('/')->with('success', 'Login succesfully');
                        } else {
                            return redirect('/')->with('success', 'You are not authorized');
                        }
                    } else {
                        $error = 'Email or Password is incorrect';
                        LoginHistory::add($request, LoginHistory::TYPE_WEB, LoginHistory::STATE_FAILED, $error);
                        return Redirect::back()->withInput()->with('error', $error);
                    }
                } else {
                    $error = 'User is not active';
                    return Redirect::back()->withInput()->with('error', $error);
                }
            } else {
                $error = 'Email does not exist';
                LoginHistory::add($request, LoginHistory::TYPE_WEB, LoginHistory::STATE_FAILED, $error);
                return Redirect::back()->withInput()->with('error', $error);
            }
        }
    }
    
    public function forgotPassword(Request $request)
    {
        $rules = array(
            'email' => 'required|email'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $user = User::where('email', $request->input('email'))->first();
            if (! empty($user)) {
                $user->getResetUrl();
                $res = EmailQueue::add([
                    'to' => $user->email,
                    'from' => 'admin@toxsl.in',
                    'subject' => 'Reset Password Email',
                    'view' => 'email.forgot-password',
                    'model' => [
                        'user' => $user
                    ]
                ]);
                if ($res) {
                    return Redirect::back()->with('success', 'Mail has been sent to you with a reset link.');
                } else {
                    return Redirect::back()->with('error', 'Some error occured. Please try again later');
                }
            } else {
                return Redirect::back()->withInput()->with('error', 'Email does not exist');
            }
        }
    }
    
    public function resetPassword(Request $request, $token)
    {
        $user = User::where('password_reset_token', $token)->first();
        if (! empty($user)) {
            return view('frontend.auth.update-password');
        } else {
            return view('frontend.auth.update-password', [
                'error' => 'This URL is expired'
            ]);
        }
    }
    
    public function updateUserPassword(Request $request, $token)
    {
        $rules = array(
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $user = User::where('password_reset_token', $token)->first();
            if (! empty($user)) {
                $user->password = bcrypt($request->input('password'));
                $user->password_reset_token = '';
                if ($user->save()) {
                    return Redirect::back()->with('success', 'Password updated succesfully');
                } else {
                    return Redirect::back()->with('error', 'Some error occured. Please try again later');
                }
            } else {
                return Redirect::back()->with('error', 'This URL is expired');
            }
        }
    }
}